#!/usr/bin/env bash

################################################################################################################
#
#                   Script d'install d'un serveur, traduction du Dockerfile.
#
################################################################################################################

usage() {
  cat << EOF
Script d'install d'un serveur, traduction du Dockerfile.
Usage: $0 <version de PHP>
Exemple: $0 7.3
EOF
  exit 0;
}

[[ -z "$1" ]] && usage

################################################################################################################

PHP_VERSION=$1

if [ "$1" == "5.6" ]; then
  . Dockerfile-5.x.sh $1
elif [ "$1" == "7.0" ]; then
  . Dockerfile-7.x.sh $1
elif [ "$1" == "7.1" ]; then
  . Dockerfile-7.x.sh $1
elif [ "$1" == "7.2" ]; then
  . Dockerfile-7.x.sh $1
elif [ "$1" == "7.3" ]; then
  . Dockerfile-7.x.sh $1
elif [ "$1" == "7.4" ]; then
  . Dockerfile-7.x.sh $1
else
  . Dockerfile-8.x.sh $1
fi
