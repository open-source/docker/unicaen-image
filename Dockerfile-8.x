###########################################################################################
#
#                               Image Unicaen
#
#                                PHP 8.x
#
###########################################################################################

#
# NB: Derrière un proxy, transmission nécessaire des variables d'env `http_*` via `--build-arg` :
#   --build-arg http_proxy
#   --build-arg https_proxy
#   --build-arg no_proxy
#

FROM composer:2.3.10 AS get-composer

########################################

FROM debian:bullseye AS distrib
#FROM localhost:5000/bullseye AS distrib

LABEL maintainer="Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>"

ARG PHP_VERSION

# Variables d'ENVironnement nécessaires au build
ENV PHP_VERSION=${PHP_VERSION} \
    APACHE_CONF_DIR=/etc/apache2 \
    PHP_CONF_DIR=/etc/php/${PHP_VERSION}

ENV HTTP_PROXY=${http_proxy} \
    HTTPS_PROXY=${https_proxy} \
    NO_PROXY=${no_proxy} \
    http_proxy=${http_proxy} \
    https_proxy=${https_proxy} \
    no_proxy=${no_proxy}

ENV APACHE_CONF_LOCAL_DIR=configs/apache \
    PHP_CONF_LOCAL_DIR=configs/php/8.x

# Installation des packages communs
RUN apt-get -qq update && \
    apt-get install -y \
        apache2 \
        ca-certificates \
        curl \
        ghostscript \
        ghostscript-x \
        git \
        imagemagick \
        ldap-utils \
        libaio1 \
        libcurl4-openssl-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg62-turbo-dev \
        libldap2-dev \
        libmcrypt-dev \
        libmemcached-dev \
        libmemcached-tools \
        libssl-dev \
        libxml2-dev \
        make \
        memcached \
        nano \
        netcat-openbsd \
        ssh \
        ssl-cert \
        subversion \
        unzip \
        vim \
        wget \
        zlib1g-dev

########################################

FROM distrib AS php

# Repositories fournissant PHP 5.x, 7.x et 8.x
RUN apt-get -qq update && \
    apt-get -y install apt-transport-https lsb-release ca-certificates curl && \
    curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg && \
    sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
#    wget --no-check-certificate -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
#    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list

RUN apt-get -qq update && \
    apt-get install -y \
        php-pear \
        php${PHP_VERSION} \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-dev \
        php${PHP_VERSION}-fpm \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-gettext \
        php${PHP_VERSION}-iconv \
        php${PHP_VERSION}-imagick \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-ldap \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-memcached \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-pgsql \
        php${PHP_VERSION}-soap \
#        php${PHP_VERSION}-xdebug \  # (cf. install manuelle ci-après)
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-readline

# Les versions 3.3.0 et 3.3.1 de xdebug fontt planter l'appli (erreur 503) en PHP 8.0 donc install de la version 3.2.2
RUN pecl install xdebug-3.2.2 && \
    echo 'zend_extension=xdebug.so' > /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini && \
    echo 'zend_extension=xdebug.so' > /etc/php/${PHP_VERSION}/fpm/conf.d/20-xdebug.ini

# Forçage de la version de PHP CLI
RUN update-alternatives --set php /usr/bin/php${PHP_VERSION}

# Config PHP
ADD ${PHP_CONF_LOCAL_DIR}/90-unicaen.ini     ${PHP_CONF_DIR}/fpm/conf.d/90-unicaen.ini
ADD ${PHP_CONF_LOCAL_DIR}/91-unicaen-dev.ini ${PHP_CONF_DIR}/fpm/conf.d/91-unicaen-dev.ini

# Composer
COPY --from=get-composer /usr/bin/composer /usr/local/bin/composer

########################################

FROM php AS apache

RUN apt-get -qq update && \
    apt-get install -y \
        apache2

RUN a2enmod actions alias rewrite ssl proxy proxy_fcgi setenvif headers && \
    a2dismod mpm_event && a2enmod mpm_worker
ADD ${APACHE_CONF_LOCAL_DIR}/security.conf ${APACHE_CONF_DIR}/conf-available/security-unicaen.conf
RUN a2disconf security.conf && \
    a2enconf security-unicaen.conf php${PHP_VERSION}-fpm


# Nettoyage
RUN apt-get autoremove -y && apt-get clean

# Copie les fichiers situés dans ./entrypoint.d dans le dossier /entrypoint.d de l'image.
# Les scripts exécutables parmi eux seront exécutés au démarrage du container (cf. entrypoint.sh).
# Attention : les noms de fichiers ne doivent être constitués que de lettres minuscules ou majuscules,
# de chiffres, de tirets bas (underscore) ou de tirets ; extension interdite, donc.
ADD entrypoint.d/* /entrypoint.d/

# Entry point
ADD entrypoint.sh /sbin/entrypoint.sh
RUN chmod 755 /sbin/entrypoint.sh
CMD ["/sbin/entrypoint.sh"]
