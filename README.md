# Image Docker Unicaen

L'ambition de cette image Docker est de tenir lieu d'image de base pour les applis que nous développons. 
Elle consigne nos pratiques communes en terme de packages debian installés, de config PHP, apache, etc.

## Obtention de l'image 

```bash
git clone https://git.unicaen.fr/open-source/docker/unicaen-image.git
cd unicaen-image
```

## Construction de l'image (build)

Construisez l'image pour la version de PHP désirée, pour l'ajouter au registre des images Docker... 

### PHP 5.6

```bash
PHP_VERSION=5.6 ; \
docker build \
--rm \
--build-arg PHP_VERSION=${PHP_VERSION} \
--build-arg OCI8_PACKAGE="oci8-2.0.12" \
-f Dockerfile-5.x \
-t unicaen-dev-php${PHP_VERSION}-apache \
.
```

*Si vous êtes derrière un proxy, reportez-vous au paragraphe [Proxy](#Proxy).*

### PHP 7.x

Exemple pour PHP 7.4 :
```bash
PHP_VERSION=7.4 ; \
docker build \
--rm \
--build-arg PHP_VERSION=${PHP_VERSION} \
-f Dockerfile-7.x \
-t unicaen-dev-php${PHP_VERSION}-apache \
.
```

*Si vous êtes derrière un proxy, reportez-vous au paragraphe [Proxy](#Proxy).*

### PHP 8.x

Exemple pour PHP 8.0 :
```bash
PHP_VERSION=8.0 ; \
docker build \
--rm \
--build-arg PHP_VERSION=${PHP_VERSION} \
-f Dockerfile-8.x \
-t unicaen-dev-php${PHP_VERSION}-apache \
.
```

### Proxy

Si vous êtes derrière un proxy, passez les variables `*_proxy` à la commande `build` avec des `--build-arg` additionnels.

Exemple :
```bash
--build-arg http_proxy=http://proxy.unicaen.fr:3128 \
--build-arg https_proxy=http://proxy.unicaen.fr:3128 \
--build-arg no_proxy=*.unicaen.fr \
```


## Utilisation de l'image 

### Dans un `Dockerfile`

Exemple, avec l'image Unicaen en PHP 7.1 :

    FROM unicaen-dev-php7.1-apache
    
    ## Installation de packages supplémentaires
    RUN apt-get update && apt-get install -y \
        unoconv
        
    # Symlink apache access and error logs to stdout/stderr so Docker logs shows them
    RUN ln -sf /dev/stdout /var/log/apache2/access.log
    RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log
    RUN ln -sf /dev/stderr /var/log/apache2/error.log
    
    # Configuration Apache et FPM
    ADD docker/apache-ports.conf    ${APACHE_CONF_DIR}/ports.conf
    ADD docker/apache-site.conf     ${APACHE_CONF_DIR}/sites-available/app.conf
    ADD docker/apache-site-ssl.conf ${APACHE_CONF_DIR}/sites-available/app-ssl.conf
    ADD docker/fpm/pool.d/app.conf  ${PHP_CONF_DIR}/fpm/pool.d/app.conf
    ADD docker/fpm/conf.d/app.ini   ${PHP_CONF_DIR}/fpm/conf.d/app.ini
    
    # Décommenter pour copier les fichiers situés dans docker/entrypoint.d dans le dossier /entrypoint.d de l'image.
    # Les scripts exécutables parmi eux seront exécutés au démarrage du container (cf. entrypoint.sh).
    # Attention : les noms de fichiers ne doivent être constitués que de lettres minuscules ou majuscules,
    # de chiffres, de tirets bas (underscore) ou de tirets ; extension interdite, donc.
    #COPY docker/entrypoint.d/* /entrypoint.d/
    
    RUN a2ensite app app-ssl && service php7.1-fpm reload

### Run 

- Exemple : démarrage du container pour tester une appli en local

```bash
PHP_VERSION=7.3 ; \
docker run \
--rm \
-d \
-p 8080:80 \
-p 8443:443 \
--volume /workspace/sygal:/var/www/webapp \
--name sygal-dev-php${PHP_VERSION}-apache \
unicaen-dev-php${PHP_VERSION}-apache
```

Une fois le container démarré ainsi, visiter l'URL `https://localhost:8443`.

- Exemple : démarrage d'un container et lancement d'un `composer update`

```bash
PHP_VERSION=7.3 ; \
docker run \
--rm \
-it \
--volume ${PWD}:/var/www/webapp \
--workdir /var/www/webapp \
unicaen-dev-php${PHP_VERSION}-apache \
composer install
```

### Exec 

- Lancement d'une commande dans un container déjà démarré, exemple :

```bash
docker exec \
sygal-dev-php7.0-apache \
php -v
```
