#!/usr/bin/env bash

################################################################################################################
#
#                   Script d'install d'un serveur, traduction du Dockerfile.
#
#                                         PHP 5.6
#
################################################################################################################

usage() {
  cat << EOF
Script d'install d'un serveur, traduction du Dockerfile.
Usage: $0 <version de PHP>
EOF
  exit 0;
}

[[ -z "$1" ]] && usage

################################################################################################################

#export http_proxy=
#export https_proxy=
#export no_proxy=

export PHP_VERSION="$1"
export OCI8_PACKAGE="oci8-2.0.12"
export APACHE_CONF_DIR=/etc/apache2
export PHP_CONF_DIR=/etc/php/${PHP_VERSION}

export APACHE_CONF_LOCAL_DIR=configs/apache
export PHP_CONF_LOCAL_DIR=configs/php/5.x

# Mise à niveau de la distrib
#echo "Acquire::http::proxy \"${http_proxy}\";" >> /etc/apt/apt.conf.d/05proxy
#echo "Acquire::https::proxy \"${https_proxy}\";" >> /etc/apt/apt.conf.d/05proxy
apt -qq update && apt -y full-upgrade

# PHP 7.x PPA repository
apt install -y wget lsb-release apt-transport-https ca-certificates
#echo "http_proxy = ${http_proxy}"  >> /etc/wgetrc
#echo "https_proxy = ${https_proxy}" >> /etc/wgetrc
#echo "no_proxy = ${no_proxy}" >> /etc/wgetrc
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list

# Installation des packages requis.
apt -qq update && \
    apt install -y \
        apache2 \
        ca-certificates \
        curl \
        ghostscript \
        ghostscript-x \
        git \
        ldap-utils \
        libaio1 \
        libcurl4-openssl-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg62-turbo-dev \
        libldap2-dev \
        libmcrypt-dev \
        libmemcached-dev \
        libssl-dev \
        libxml2-dev \
        make \
        memcached \
        libmemcached-tools \
        netcat-openbsd \
        php-pear \
        php${PHP_VERSION} \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-dev \
        php${PHP_VERSION}-fpm \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-gettext \
        php${PHP_VERSION}-iconv \
        php${PHP_VERSION}-imagick \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-ldap \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-memcached \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-pgsql \
        php${PHP_VERSION}-soap \
        php${PHP_VERSION}-xdebug \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-json \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-readline \
        ssh \
        ssl-cert \
        subversion \
        unzip \
        vim \
        wget \
        zlib1g-dev

# Forçage de la version de PHP CLI
update-alternatives --set php /usr/bin/php${PHP_VERSION}

# Config PHP.
cp ${PHP_CONF_LOCAL_DIR}/opcache.ini ${PHP_CONF_DIR}/fpm/conf.d/90-opcache.ini
cp ${PHP_CONF_LOCAL_DIR}/php.conf    ${PHP_CONF_DIR}/fpm/conf.d/91-unicaen.ini
cp ${PHP_CONF_LOCAL_DIR}/xdebug.conf ${PHP_CONF_DIR}/fpm/conf.d/92-xdebug.ini

# Apache
a2enmod actions alias rewrite ssl proxy proxy_fcgi setenvif headers && \
a2dismod mpm_event mpm_prefork && a2enmod mpm_worker
cp ${APACHE_CONF_LOCAL_DIR}/security.conf ${APACHE_CONF_DIR}/conf-available/security-unicaen.conf
a2disconf security.conf && \
a2enconf security-unicaen.conf php${PHP_VERSION}-fpm

# Composer
curl -sS https://getcomposer.org/installer --proxy "${http_proxy}" | php -- --install-dir=/usr/local/bin --filename=composer --version 1.10.19
composer global require hirak/prestissimo --no-plugins --no-scripts

# Nettoyage
apt autoremove -y && apt clean

# Copie les fichiers situés dans ./entrypoint.d dans le dossier /entrypoint.d de l'image.
# Les scripts exécutables parmi eux seront exécutés au démarrage du container (cf. entrypoint.sh).
# Attention : les noms de fichiers ne doivent être constitués que de lettres minuscules ou majuscules,
# de chiffres, de tirets bas (underscore) ou de tirets ; extension interdite, donc.
mkdir -p /entrypoint.d
cp entrypoint.d/* /entrypoint.d/

# Entry point
cp entrypoint.sh /sbin/entrypoint.sh
chmod 755 /sbin/entrypoint.sh
export WITHOUT_DOCKER=1
bash /sbin/entrypoint.sh
