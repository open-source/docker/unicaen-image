# CHANGELOG

## 1.1.1

- Réorganisation des configs PHP par version car la config de xdebug v2 (PHP 5.x) n'est pas pareille à celle de la v3 (PHP 7.x, 8.x).

## 1.1.0

- Correction des Dockerfile pour forcer l'install de composer v1 (car la v2 génère des problèmes).
- Correction de la conf PHP pour xdebug v3.

## 1.0.0

- Un Dockerfile par version majeure de PHP car les versions du driver Oracle OCI8 et Instant Client diffèrent.
