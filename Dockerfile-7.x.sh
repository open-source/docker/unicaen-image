#!/usr/bin/env bash

################################################################################################################
#
#                   Script d'install d'un serveur, traduction du Dockerfile.
#
#                                         PHP 7.x
#
################################################################################################################

usage() {
  cat << EOF
Script d'install d'un serveur, traduction du Dockerfile.
Usage: $0 <version de PHP>
EOF
  exit 0;
}

[[ -z "$1" ]] && usage

################################################################################################################

#export http_proxy=
#export https_proxy=
#export no_proxy=

export PHP_VERSION="$1"
export APACHE_CONF_DIR=/etc/apache2
export PHP_CONF_DIR=/etc/php/${PHP_VERSION}

export APACHE_CONF_LOCAL_DIR=configs/apache
export PHP_CONF_LOCAL_DIR=configs/php/7.x

# Mise à niveau de la distrib
#echo "Acquire::http::proxy \"${http_proxy}\";" >> /etc/apt/apt.conf.d/05proxy
#echo "Acquire::https::proxy \"${https_proxy}\";" >> /etc/apt/apt.conf.d/05proxy
sudo -E apt -qq update && apt -y full-upgrade

# PHP 7.x PPA repository
sudo -E apt install -y wget lsb-release apt-transport-https ca-certificates
#echo "http_proxy = ${http_proxy}"  >> /etc/wgetrc
#echo "https_proxy = ${https_proxy}" >> /etc/wgetrc
#echo "no_proxy = ${no_proxy}" >> /etc/wgetrc
sudo -E wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

# Installation des packages requis.
sudo -E apt install -y \
    apache2 \
    ca-certificates \
    curl \
    ghostscript \
    ghostscript-x \
    git \
    ldap-utils \
    libaio1 \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libicu-dev \
    libjpeg62-turbo-dev \
    libldap2-dev \
    libmcrypt-dev \
    libmemcached-dev \
    libssl-dev \
    libxml2-dev \
    make \
    memcached \
    libmemcached-tools \
    netcat-openbsd \
    php-pear \
    php${PHP_VERSION} \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-dev \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-gettext \
    php${PHP_VERSION}-iconv \
    php${PHP_VERSION}-imagick \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-ldap \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-memcached \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-opcache \
    php${PHP_VERSION}-pgsql \
    php${PHP_VERSION}-soap \
    php${PHP_VERSION}-xdebug \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-common \
    php${PHP_VERSION}-json \
    php${PHP_VERSION}-opcache \
    php${PHP_VERSION}-readline \
    ssh \
    ssl-cert \
    subversion \
    unzip \
    vim \
    wget \
    zlib1g-dev

# Forçage de la version de PHP CLI
sudo update-alternatives --set php /usr/bin/php${PHP_VERSION}

# Config PHP.
sudo cp ${PHP_CONF_LOCAL_DIR}/90-unicaen.ini     ${PHP_CONF_DIR}/fpm/conf.d/90-unicaen.ini
sudo cp ${PHP_CONF_LOCAL_DIR}/91-unicaen-dev.ini ${PHP_CONF_DIR}/fpm/conf.d/91-unicaen-dev.ini

# Apache
sudo a2enmod actions alias rewrite ssl proxy proxy_fcgi setenvif headers && \
sudo a2dismod mpm_event mpm_prefork && \
sudo a2enmod mpm_worker
sudo cp ${APACHE_CONF_LOCAL_DIR}/security.conf ${APACHE_CONF_DIR}/conf-available/security-unicaen.conf
sudo a2disconf security.conf && \
sudo a2enconf security-unicaen.conf php${PHP_VERSION}-fpm

# Composer
sudo -E curl -sS https://getcomposer.org/installer | sudo -E php -- --install-dir=/usr/local/bin --filename=composer

# Nettoyage
sudo apt autoremove -y && sudo apt clean
